/**
 * @license Angular v9.1.0-rc.0+110.sha-c8bef12
 * (c) 2010-2020 Google LLC. https://angular.io/
 * License: MIT
 */

export * from './browser/browser';
