/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/** Tracks state of resize events in progress. */
var ResizeRef = /** @class */ (function () {
    function ResizeRef(origin, overlayRef, minWidthPx, maxWidthPx) {
        this.origin = origin;
        this.overlayRef = overlayRef;
        this.minWidthPx = minWidthPx;
        this.maxWidthPx = maxWidthPx;
    }
    return ResizeRef;
}());
export { ResizeRef };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzaXplLXJlZi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jZGstZXhwZXJpbWVudGFsL2NvbHVtbi1yZXNpemUvcmVzaXplLXJlZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7O0dBTUc7QUFLSCxpREFBaUQ7QUFDakQ7SUFDRSxtQkFDYSxNQUFrQixFQUNsQixVQUFzQixFQUN0QixVQUFrQixFQUNsQixVQUFrQjtRQUhsQixXQUFNLEdBQU4sTUFBTSxDQUFZO1FBQ2xCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUNsQixlQUFVLEdBQVYsVUFBVSxDQUFRO0lBQUssQ0FBQztJQUN2QyxnQkFBQztBQUFELENBQUMsQUFORCxJQU1DIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEdvb2dsZSBMTEMgQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqXG4gKiBVc2Ugb2YgdGhpcyBzb3VyY2UgY29kZSBpcyBnb3Zlcm5lZCBieSBhbiBNSVQtc3R5bGUgbGljZW5zZSB0aGF0IGNhbiBiZVxuICogZm91bmQgaW4gdGhlIExJQ0VOU0UgZmlsZSBhdCBodHRwczovL2FuZ3VsYXIuaW8vbGljZW5zZVxuICovXG5cbmltcG9ydCB7RWxlbWVudFJlZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge092ZXJsYXlSZWZ9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vdmVybGF5JztcblxuLyoqIFRyYWNrcyBzdGF0ZSBvZiByZXNpemUgZXZlbnRzIGluIHByb2dyZXNzLiAqL1xuZXhwb3J0IGNsYXNzIFJlc2l6ZVJlZiB7XG4gIGNvbnN0cnVjdG9yKFxuICAgICAgcmVhZG9ubHkgb3JpZ2luOiBFbGVtZW50UmVmLFxuICAgICAgcmVhZG9ubHkgb3ZlcmxheVJlZjogT3ZlcmxheVJlZixcbiAgICAgIHJlYWRvbmx5IG1pbldpZHRoUHg6IG51bWJlcixcbiAgICAgIHJlYWRvbmx5IG1heFdpZHRoUHg6IG51bWJlciwgKSB7fVxufVxuIl19