/**
 * @fileoverview added by tsickle
 * Generated from: src/cdk-experimental/column-resize/resize-ref.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Tracks state of resize events in progress.
 */
export class ResizeRef {
    /**
     * @param {?} origin
     * @param {?} overlayRef
     * @param {?} minWidthPx
     * @param {?} maxWidthPx
     */
    constructor(origin, overlayRef, minWidthPx, maxWidthPx) {
        this.origin = origin;
        this.overlayRef = overlayRef;
        this.minWidthPx = minWidthPx;
        this.maxWidthPx = maxWidthPx;
    }
}
if (false) {
    /** @type {?} */
    ResizeRef.prototype.origin;
    /** @type {?} */
    ResizeRef.prototype.overlayRef;
    /** @type {?} */
    ResizeRef.prototype.minWidthPx;
    /** @type {?} */
    ResizeRef.prototype.maxWidthPx;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzaXplLXJlZi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jZGstZXhwZXJpbWVudGFsL2NvbHVtbi1yZXNpemUvcmVzaXplLXJlZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFZQSxNQUFNLE9BQU8sU0FBUzs7Ozs7OztJQUNwQixZQUNhLE1BQWtCLEVBQ2xCLFVBQXNCLEVBQ3RCLFVBQWtCLEVBQ2xCLFVBQWtCO1FBSGxCLFdBQU0sR0FBTixNQUFNLENBQVk7UUFDbEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixlQUFVLEdBQVYsVUFBVSxDQUFRO1FBQ2xCLGVBQVUsR0FBVixVQUFVLENBQVE7SUFBSyxDQUFDO0NBQ3RDOzs7SUFKSywyQkFBMkI7O0lBQzNCLCtCQUErQjs7SUFDL0IsK0JBQTJCOztJQUMzQiwrQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgR29vZ2xlIExMQyBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICpcbiAqIFVzZSBvZiB0aGlzIHNvdXJjZSBjb2RlIGlzIGdvdmVybmVkIGJ5IGFuIE1JVC1zdHlsZSBsaWNlbnNlIHRoYXQgY2FuIGJlXG4gKiBmb3VuZCBpbiB0aGUgTElDRU5TRSBmaWxlIGF0IGh0dHBzOi8vYW5ndWxhci5pby9saWNlbnNlXG4gKi9cblxuaW1wb3J0IHtFbGVtZW50UmVmfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7T3ZlcmxheVJlZn0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuXG4vKiogVHJhY2tzIHN0YXRlIG9mIHJlc2l6ZSBldmVudHMgaW4gcHJvZ3Jlc3MuICovXG5leHBvcnQgY2xhc3MgUmVzaXplUmVmIHtcbiAgY29uc3RydWN0b3IoXG4gICAgICByZWFkb25seSBvcmlnaW46IEVsZW1lbnRSZWYsXG4gICAgICByZWFkb25seSBvdmVybGF5UmVmOiBPdmVybGF5UmVmLFxuICAgICAgcmVhZG9ubHkgbWluV2lkdGhQeDogbnVtYmVyLFxuICAgICAgcmVhZG9ubHkgbWF4V2lkdGhQeDogbnVtYmVyLCApIHt9XG59XG4iXX0=