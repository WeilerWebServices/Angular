/**
 * @fileoverview added by tsickle
 * Generated from: src/cdk-experimental/column-resize/selectors.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
// TODO: Figure out how to remove `mat-` classes from the CDK part of the
// column resize implementation.
/** @type {?} */
export const HEADER_CELL_SELECTOR = '.cdk-header-cell, .mat-header-cell';
/** @type {?} */
export const HEADER_ROW_SELECTOR = '.cdk-header-row, .mat-header-row';
/** @type {?} */
export const RESIZE_OVERLAY_SELECTOR = '.mat-column-resize-overlay-thumb';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3JzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vc3JjL2Nkay1leHBlcmltZW50YWwvY29sdW1uLXJlc2l6ZS9zZWxlY3RvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBV0EsTUFBTSxPQUFPLG9CQUFvQixHQUFHLG9DQUFvQzs7QUFFeEUsTUFBTSxPQUFPLG1CQUFtQixHQUFHLGtDQUFrQzs7QUFFckUsTUFBTSxPQUFPLHVCQUF1QixHQUFHLGtDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBHb29nbGUgTExDIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKlxuICogVXNlIG9mIHRoaXMgc291cmNlIGNvZGUgaXMgZ292ZXJuZWQgYnkgYW4gTUlULXN0eWxlIGxpY2Vuc2UgdGhhdCBjYW4gYmVcbiAqIGZvdW5kIGluIHRoZSBMSUNFTlNFIGZpbGUgYXQgaHR0cHM6Ly9hbmd1bGFyLmlvL2xpY2Vuc2VcbiAqL1xuXG4vLyBUT0RPOiBGaWd1cmUgb3V0IGhvdyB0byByZW1vdmUgYG1hdC1gIGNsYXNzZXMgZnJvbSB0aGUgQ0RLIHBhcnQgb2YgdGhlXG4vLyBjb2x1bW4gcmVzaXplIGltcGxlbWVudGF0aW9uLlxuXG5leHBvcnQgY29uc3QgSEVBREVSX0NFTExfU0VMRUNUT1IgPSAnLmNkay1oZWFkZXItY2VsbCwgLm1hdC1oZWFkZXItY2VsbCc7XG5cbmV4cG9ydCBjb25zdCBIRUFERVJfUk9XX1NFTEVDVE9SID0gJy5jZGstaGVhZGVyLXJvdywgLm1hdC1oZWFkZXItcm93JztcblxuZXhwb3J0IGNvbnN0IFJFU0laRV9PVkVSTEFZX1NFTEVDVE9SID0gJy5tYXQtY29sdW1uLXJlc2l6ZS1vdmVybGF5LXRodW1iJztcbiJdfQ==