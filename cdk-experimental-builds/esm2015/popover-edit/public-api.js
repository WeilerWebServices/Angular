/**
 * @fileoverview added by tsickle
 * Generated from: src/cdk-experimental/popover-edit/public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
export { EditEventDispatcher } from './edit-event-dispatcher';
export { EditRef } from './edit-ref';
export { FocusDispatcher } from './focus-dispatcher';
export { FormValueContainer } from './form-value-container';
export { CdkEditControl, CdkEditRevert, CdkEditClose } from './lens-directives';
export { CdkPopoverEditModule } from './popover-edit-module';
export { PopoverEditPositionStrategyFactory, DefaultPopoverEditPositionStrategyFactory } from './popover-edit-position-strategy-factory';
export { CdkEditable, CdkPopoverEdit, CdkPopoverEditTabOut, CdkRowHoverContent, CdkEditOpen } from './table-directives';
// Private to Angular Components
export { CELL_SELECTOR as _CELL_SELECTOR } from './constants';
export { closest as _closest, matches as _matches } from './polyfill';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jZGstZXhwZXJpbWVudGFsL3BvcG92ZXItZWRpdC9wdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQVFBLG9DQUFjLHlCQUF5QixDQUFDO0FBQ3hDLHdCQUFjLFlBQVksQ0FBQztBQUMzQixnQ0FBYyxvQkFBb0IsQ0FBQztBQUNuQyxtQ0FBYyx3QkFBd0IsQ0FBQztBQUN2Qyw0REFBYyxtQkFBbUIsQ0FBQztBQUNsQyxxQ0FBYyx1QkFBdUIsQ0FBQztBQUN0Qyw4RkFBYywwQ0FBMEMsQ0FBQztBQUN6RCxtR0FBYyxvQkFBb0IsQ0FBQzs7QUFHbkMsT0FBTyxFQUFDLGFBQWEsSUFBSSxjQUFjLEVBQUMsTUFBTSxhQUFhLENBQUM7QUFDNUQsT0FBTyxFQUFDLE9BQU8sSUFBSSxRQUFRLEVBQUUsT0FBTyxJQUFJLFFBQVEsRUFBQyxNQUFNLFlBQVksQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBHb29nbGUgTExDIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKlxuICogVXNlIG9mIHRoaXMgc291cmNlIGNvZGUgaXMgZ292ZXJuZWQgYnkgYW4gTUlULXN0eWxlIGxpY2Vuc2UgdGhhdCBjYW4gYmVcbiAqIGZvdW5kIGluIHRoZSBMSUNFTlNFIGZpbGUgYXQgaHR0cHM6Ly9hbmd1bGFyLmlvL2xpY2Vuc2VcbiAqL1xuXG5leHBvcnQgKiBmcm9tICcuL2VkaXQtZXZlbnQtZGlzcGF0Y2hlcic7XG5leHBvcnQgKiBmcm9tICcuL2VkaXQtcmVmJztcbmV4cG9ydCAqIGZyb20gJy4vZm9jdXMtZGlzcGF0Y2hlcic7XG5leHBvcnQgKiBmcm9tICcuL2Zvcm0tdmFsdWUtY29udGFpbmVyJztcbmV4cG9ydCAqIGZyb20gJy4vbGVucy1kaXJlY3RpdmVzJztcbmV4cG9ydCAqIGZyb20gJy4vcG9wb3Zlci1lZGl0LW1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL3BvcG92ZXItZWRpdC1wb3NpdGlvbi1zdHJhdGVneS1mYWN0b3J5JztcbmV4cG9ydCAqIGZyb20gJy4vdGFibGUtZGlyZWN0aXZlcyc7XG5cbi8vIFByaXZhdGUgdG8gQW5ndWxhciBDb21wb25lbnRzXG5leHBvcnQge0NFTExfU0VMRUNUT1IgYXMgX0NFTExfU0VMRUNUT1J9IGZyb20gJy4vY29uc3RhbnRzJztcbmV4cG9ydCB7Y2xvc2VzdCBhcyBfY2xvc2VzdCwgbWF0Y2hlcyBhcyBfbWF0Y2hlc30gZnJvbSAnLi9wb2x5ZmlsbCc7XG4iXX0=