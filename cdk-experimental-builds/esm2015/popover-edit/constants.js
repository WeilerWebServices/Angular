/**
 * @fileoverview added by tsickle
 * Generated from: src/cdk-experimental/popover-edit/constants.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Selector for finding table cells.
 * @type {?}
 */
export const CELL_SELECTOR = '.cdk-cell, .mat-cell, td';
/**
 * Selector for finding editable table cells.
 * @type {?}
 */
export const EDITABLE_CELL_SELECTOR = '.cdk-popover-edit-cell, .mat-popover-edit-cell';
/**
 * Selector for finding table rows.
 * @type {?}
 */
export const ROW_SELECTOR = '.cdk-row, .mat-row, tr';
/**
 * Selector for finding the table element.
 * @type {?}
 */
export const TABLE_SELECTOR = 'table, cdk-table, mat-table';
/**
 * CSS class added to the edit lens pane.
 * @type {?}
 */
export const EDIT_PANE_CLASS = 'cdk-edit-pane';
/**
 * Selector for finding the edit lens pane.
 * @type {?}
 */
export const EDIT_PANE_SELECTOR = `.${EDIT_PANE_CLASS}, .mat-edit-pane`;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vc3JjL2Nkay1leHBlcmltZW50YWwvcG9wb3Zlci1lZGl0L2NvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBU0EsTUFBTSxPQUFPLGFBQWEsR0FBRywwQkFBMEI7Ozs7O0FBR3ZELE1BQU0sT0FBTyxzQkFBc0IsR0FBRyxnREFBZ0Q7Ozs7O0FBR3RGLE1BQU0sT0FBTyxZQUFZLEdBQUcsd0JBQXdCOzs7OztBQUdwRCxNQUFNLE9BQU8sY0FBYyxHQUFHLDZCQUE2Qjs7Ozs7QUFHM0QsTUFBTSxPQUFPLGVBQWUsR0FBRyxlQUFlOzs7OztBQUc5QyxNQUFNLE9BQU8sa0JBQWtCLEdBQUcsSUFBSSxlQUFlLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBHb29nbGUgTExDIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKlxuICogVXNlIG9mIHRoaXMgc291cmNlIGNvZGUgaXMgZ292ZXJuZWQgYnkgYW4gTUlULXN0eWxlIGxpY2Vuc2UgdGhhdCBjYW4gYmVcbiAqIGZvdW5kIGluIHRoZSBMSUNFTlNFIGZpbGUgYXQgaHR0cHM6Ly9hbmd1bGFyLmlvL2xpY2Vuc2VcbiAqL1xuXG4vKiogU2VsZWN0b3IgZm9yIGZpbmRpbmcgdGFibGUgY2VsbHMuICovXG5leHBvcnQgY29uc3QgQ0VMTF9TRUxFQ1RPUiA9ICcuY2RrLWNlbGwsIC5tYXQtY2VsbCwgdGQnO1xuXG4vKiogU2VsZWN0b3IgZm9yIGZpbmRpbmcgZWRpdGFibGUgdGFibGUgY2VsbHMuICovXG5leHBvcnQgY29uc3QgRURJVEFCTEVfQ0VMTF9TRUxFQ1RPUiA9ICcuY2RrLXBvcG92ZXItZWRpdC1jZWxsLCAubWF0LXBvcG92ZXItZWRpdC1jZWxsJztcblxuLyoqIFNlbGVjdG9yIGZvciBmaW5kaW5nIHRhYmxlIHJvd3MuICovXG5leHBvcnQgY29uc3QgUk9XX1NFTEVDVE9SID0gJy5jZGstcm93LCAubWF0LXJvdywgdHInO1xuXG4vKiogU2VsZWN0b3IgZm9yIGZpbmRpbmcgdGhlIHRhYmxlIGVsZW1lbnQuICovXG5leHBvcnQgY29uc3QgVEFCTEVfU0VMRUNUT1IgPSAndGFibGUsIGNkay10YWJsZSwgbWF0LXRhYmxlJztcblxuLyoqIENTUyBjbGFzcyBhZGRlZCB0byB0aGUgZWRpdCBsZW5zIHBhbmUuICovXG5leHBvcnQgY29uc3QgRURJVF9QQU5FX0NMQVNTID0gJ2Nkay1lZGl0LXBhbmUnO1xuXG4vKiogU2VsZWN0b3IgZm9yIGZpbmRpbmcgdGhlIGVkaXQgbGVucyBwYW5lLiAqL1xuZXhwb3J0IGNvbnN0IEVESVRfUEFORV9TRUxFQ1RPUiA9IGAuJHtFRElUX1BBTkVfQ0xBU1N9LCAubWF0LWVkaXQtcGFuZWA7XG4iXX0=