/**
 * @fileoverview added by tsickle
 * Generated from: src/cdk-experimental/dialog/public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
export { throwDialogContentAlreadyAttachedError, CdkDialogContainer } from './dialog-container';
export { DialogConfig } from './dialog-config';
export { DialogRef } from './dialog-ref';
export { Dialog } from './dialog';
export { DialogModule } from './dialog-module';
export { MAT_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY, DIALOG_SCROLL_STRATEGY, DIALOG_DATA, DIALOG_REF, DIALOG_CONFIG, DIALOG_CONTAINER, MAT_DIALOG_SCROLL_STRATEGY_PROVIDER } from './dialog-injectors';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9jZGstZXhwZXJpbWVudGFsL2RpYWxvZy9wdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQVFBLDJFQUFjLG9CQUFvQixDQUFDO0FBQ25DLDZCQUFjLGlCQUFpQixDQUFDO0FBQ2hDLDBCQUFjLGNBQWMsQ0FBQztBQUM3Qix1QkFBYyxVQUFVLENBQUM7QUFDekIsNkJBQWMsaUJBQWlCLENBQUM7QUFDaEMsbUxBQWMsb0JBQW9CLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgR29vZ2xlIExMQyBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICpcbiAqIFVzZSBvZiB0aGlzIHNvdXJjZSBjb2RlIGlzIGdvdmVybmVkIGJ5IGFuIE1JVC1zdHlsZSBsaWNlbnNlIHRoYXQgY2FuIGJlXG4gKiBmb3VuZCBpbiB0aGUgTElDRU5TRSBmaWxlIGF0IGh0dHBzOi8vYW5ndWxhci5pby9saWNlbnNlXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9kaWFsb2ctY29udGFpbmVyJztcbmV4cG9ydCAqIGZyb20gJy4vZGlhbG9nLWNvbmZpZyc7XG5leHBvcnQgKiBmcm9tICcuL2RpYWxvZy1yZWYnO1xuZXhwb3J0ICogZnJvbSAnLi9kaWFsb2cnO1xuZXhwb3J0ICogZnJvbSAnLi9kaWFsb2ctbW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vZGlhbG9nLWluamVjdG9ycyc7XG4iXX0=