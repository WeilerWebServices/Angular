/// <amd-module name="@angular/dev-infra-private/commit-message/validate-file" />
/** Validate commit message at the provided file path. */
export declare function validateFile(filePath: string): void;
